import { Connect, SimpleSigner, MNID } from 'uport-connect'

// const uport = new Connect('React uPort IPFS DApp', {
//     clientId: '2omYPjiPUiR6jyNdSZRHgbpn9PxiVqbtVsR',
//     network: 'rinkeby',
//     signer: SimpleSigner('ab1d56711bcf6f7ded8a8b7f768d59e8f3d3a168b75b037848aa142b93e4998f')
// })
const uport = new Connect('DAPP', {
  clientId: '2oiQHjYtS2xgLMMzf7cPuYkRZ7rvAubWJtj',
  network: 'rinkeby',
  signer: SimpleSigner('0eeead67955bb4f00e0348c68006324b66779010a7c4b7b6bcd67557beccb36e')
})
//public key: 0x04a2fd44e5aa5ba977a664142567ffcaecce1be62f996b3005ace6a4e43cdf9f2d5f3b577528281f4f10dc11812f6f586eefbea0901cfa4e6704bec774e9f3a60a

const initAccount = async () => {
    const user = await uport.requestCredentials({
        requested: ['name', 'country', 'avatar'],
        notifications: true // We want this if we want to recieve credentials
    })
    // get user details
    const decodedId = MNID.decode(user.address)
    const specificNetworkAddress = decodedId.address
    return { specificNetworkAddress, user }
}

const web3 = uport.getWeb3()
export { web3, uport, MNID, initAccount }



// import { Connect, SimpleSigner } from 'uport-connect'

// const uport = new Connect('DAPP', {
//   clientId: '2oiQHjYtS2xgLMMzf7cPuYkRZ7rvAubWJtj',
//   network: 'rinkeby or ropsten or kovan',
//   signer: SimpleSigner('0eeead67955bb4f00e0348c68006324b66779010a7c4b7b6bcd67557beccb36e')
// })

// // Request credentials to login
// uport.requestCredentials({
//   requested: ['name', 'phone', 'country'],
//   notifications: true // We want this if we want to recieve credentials
// })
// .then((credentials) => {
//   // Do something
// })

// // Attest specific credentials
// uport.attestCredentials({
//   sub: THE_RECEIVING_UPORT_ADDRESS,
//   claim: {
//     CREDENTIAL_NAME: CREDENTIAL_VALUE
//   },
//   exp: new Date().getTime() + 30 * 24 * 60 * 60 * 1000, // 30 days from now
// })

// import { Connect, SimpleSigner } from 'uport-connect'

// const uport = new Connect('DAPP', {
//   clientId: '2oiQHjYtS2xgLMMzf7cPuYkRZ7rvAubWJtj',
//   network: 'rinkeby or ropsten or kovan',
//   signer: SimpleSigner('0eeead67955bb4f00e0348c68006324b66779010a7c4b7b6bcd67557beccb36e')
// })

// // Request credentials to login
// uport.requestCredentials({
//   requested: ['name', 'phone', 'country'],
//   notifications: true // We want this if we want to recieve credentials
// })
// .then((credentials) => {
//   // Do something
// })

// // Attest specific credentials
// uport.attestCredentials({
//   sub: THE_RECEIVING_UPORT_ADDRESS,
//   claim: {
//     CREDENTIAL_NAME: CREDENTIAL_VALUE
//   },
//   exp: new Date().getTime() + 30 * 24 * 60 * 60 * 1000, // 30 days from now
// })